<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Attic Installations</title>
    <!-- Thanks to Pure CSS parallax scroll demo #3 by Keith Clark for the Perspective Parallax --> 
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/app.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/style.css" type="text/css"/>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/typography.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/core.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/faq.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/custom.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/media.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/style.css" />
    <link href="//cdn.rawgit.com/noelboss/featherlight/1.3.2/release/featherlight.min.css" type="text/css" rel="stylesheet" title="Featherlight Styles" />


	
   <!--Novecento-Wide-->
   	<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/stylesheet.css" type="text/css" charset="utf-8" />
   	
   	<link rel="icon" href="<?php bloginfo('siteurl'); ?>/favicon.ico" type="image/x-icon" />
   	<link rel="shortcut icon" href="<?php bloginfo('siteurl'); ?>/favicon.ico" type="image/x-icon" />

 	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
 	<script src="<?php bloginfo('stylesheet_directory'); ?>/js/faq.js"></script>
  	<script>
	 	$(function() {
		  $('a[href*=#]:not([href=#])').click(function() {
		    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		      var target = $(this.hash);
		      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		      if (target.length) {
		        $('html,body').animate({
		          scrollTop: target.offset().top
		        }, 1000);
		        return false;
		      }
		    }
		  });
		});
		
		
	$( document ).ready(function() {
	    $('#home-slider').css({ top: '40px'});
	});

	var fixed = false;
		$(document).scroll(function() {
		    if( $(window).scrollTop() >= 250 ) {
		        if( !fixed ) {
		           fixed = true;
		            $('.header').css({	top: '100px' });
		            
		            
		
		        }
		    } else {
		        if( fixed ) {
		            fixed = false; 
		            $('.header').css({	top: '0px' });
		            $('.mob-nav').css({	display: 'none' });
		        }
		    }
		});
		
	$(document).ready(
    function(){
        $(".mob-logo").click(function () {
            $(".mob-nav").toggle();
        });
    });
 
	$(document).ready(
    function(){
        $(".mob-nav").click(function () {
            $(".mob-nav").hide();
        });
    });
   
	$(document).ready(
    function(){
        $(".close").click(function () {
            $(".mob-nav").hide();
        });
    });

	</script>
		
    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>

  <div class="off-canvas-wrap">
	 <div class="inner-wrap"> 
		<section class="contact-bar heads-up">
			<p>Call Us Today: <b>09 212 9970</b></p>
			<div class="social">
				<p>Connect on:</p>
				<a href="https://www.facebook.com/atticinstallations"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/facebook.svg" alt="Facebook" /></a>
		</section> 

<!--		<header id="header" class="header scroll">				
			<nav>
				<a href="<?php echo home_url(); ?>"><img class="home-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo.svg" alt="Attic Installations" title="Attic Installations" /></a>
				<ul>
					<li><a href="<?php echo home_url(); ?>">Home</a></li>
					<li><a href="<?php echo home_url(); ?>/about-us/">About Us</a></li>
					<li><a href="<?php echo home_url(); ?>/our-work/">Our Work</a></li>
					<li><a href="<?php echo home_url(); ?>/contact-us/">Contact Us</a></li>
				</ul>
				
								
				
			</nav> 		
		</header>-->

		<div id="main-content" class="clear" role="document">
			
			<?php if ( is_page( 'Homepage' ) ){ ?>
				
				<section id="intro" class="clear">
					<section>
						<div class="overlay"></div>
						<header class="header-home clear">	
							<a href="<?php echo home_url(); ?>"><img class="home-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/footer-logo.svg" alt="Attic Installations" title="Attic Installations" /></a>
				        	<nav>
					        	<ul class="desktop">
									<li><a href="<?php echo home_url(); ?>">Home</a></li>
									<li><a href="<?php echo home_url(); ?>/about-us/">About Us</a></li>
									<li><a href="<?php echo home_url(); ?>/our-work/">Our Work</a></li>
									<li><a href="<?php echo home_url(); ?>/faq/">FAQ</a></li>
									<li><a href="<?php echo home_url(); ?>/contact-us/">Contact Us</a></li>
					        	</ul>
								<ul class="mob-nav">
									<li><a href="<?php echo home_url(); ?>">Home</a></li>
									<li><a href="<?php echo home_url(); ?>/about-us/">About Us</a></li>
									<li><a href="<?php echo home_url(); ?>/our-work/">Our Work</a></li>
									<li><a href="<?php echo home_url(); ?>/faq/">FAQ</a></li>
									<li><a href="<?php echo home_url(); ?>/contact-us/">Contact Us</a></li>
								</ul>
								<div class="mobile right mob-logo">
									<img class="menu" src="<?php bloginfo('stylesheet_directory'); ?>/images/menu-white.svg" alt="menu" />
								</div>
							</nav> 
					    </header>				
					     
					</section>
					
					<div class="cd-section">
						<h1>Attic Stair and Flooring Specialists</h1>
						<h3 style="color: white;">Giving Kiwi home owners access to the hidden storage in their homes</h3>
						<a href="mailto:sales@atticinstallations.co.nz" class="button">Get a free quote</a>
				    </div>
				    
				</section>
			
			<?php }
				else 
			{?>
			
			<section>
				<header class="header-home clear">	
					<a href="<?php echo home_url(); ?>"><img class="home-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo.svg" alt="Attic Installations" title="Attic Installations" /></a>
		        	<nav>
			        	<ul class="desktop">
							<li><a href="<?php echo home_url(); ?>">Home</a></li>
							<li><a href="<?php echo home_url(); ?>/about-us/">About Us</a></li>
							<li><a href="<?php echo home_url(); ?>/our-work/">Our Work</a></li>
							<li><a href="<?php echo home_url(); ?>/faq/">FAQ</a></li>
							<li><a href="<?php echo home_url(); ?>/contact-us/">Contact Us</a></li>
			        	</ul>
						<ul class="mob-nav">
							<li><a href="<?php echo home_url(); ?>">Home</a></li>
							<li><a href="<?php echo home_url(); ?>/about-us/">About Us</a></li>
							<li><a href="<?php echo home_url(); ?>/our-work/">Our Work</a></li>
							<li><a href="<?php echo home_url(); ?>/faq/">FAQ</a></li>
							<li><a href="<?php echo home_url(); ?>/contact-us/">Contact Us</a></li>
						</ul>
						<div class="mobile right mob-logo">
							<img class="menu" src="<?php bloginfo('stylesheet_directory'); ?>/images/menu.svg" alt="menu" />
						</div>
					
					</nav> 
			    </header>				
			     
			</section>
			  			
			<?php }
			?>

			
			
	
			