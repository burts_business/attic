			<footer class="clear white">
		        <div class="third left">
			        <img class="footer-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/footer-logo.svg" alt-"footer logo" />
		        </div>
		        <div class="third right">
			        <h4>Contact us</h4>
					<a href="tel:+64 9 212 9970">+64 9 212 9970</a>
			        <a href="mailto:sales@atticinstallations.co.nz">sales@atticinstallations.co.nz</a>
			    </div>

			</footer>		        

			
		</div><!--main content-->
		<section class="contact-bar bottom">
			<p>&#169; Copyright Attic Installations <?php echo date("Y") ?></p>
			<div class="social">
				<a class="blink" href="http://blinkltd.co.nz/" target="_blank">
				handmade by blink.
				</a>
		</section> 
	
	<a class="exit-off-canvas"></a>

  </div><!--innnerwrap-->
</div><!--off canvas wrap-->


	
	<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300' rel='stylesheet' type='text/css'>
	
	<script src="//cdn.rawgit.com/noelboss/featherlight/1.2.3/release/featherlight.min.js" type="text/javascript" charset="utf-8"></script>

  
<?php wp_footer(); ?>


</body>
</html>