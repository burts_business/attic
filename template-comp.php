<?php /* Template Name: Comp */
get_header(); ?>
<?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>    
<?php 
		
		if ( has_post_thumbnail() ) {
		 the_post_thumbnail();
		 }?>
	<section id="post-<?php the_ID(); ?>" class="cd-section clear main ">
		
		<div class="lead clear">
			<?php the_content(); ?>
		</div>

	</section>

	
          
    <?php endwhile; ?>
<?php endif; ?>
				
<?php get_footer(); ?>