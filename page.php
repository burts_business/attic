<?php get_header(); ?>
    
    <section class="cd-section clear">
	    <div class="third">
		    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icon-storage.svg" alt="clear out your garage" />
		    <h3>Your home storage solution</h3>
		    <p>Clear out your garage, unburden your closets and hide away unwanted gifts by making the most of the unused space in your home.</p>
	    </div>
	    <div class="third">
		    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icon-tree.svg" alt="clear out your garage" />
		    <h3>Store your seasonal stuff</h3>
		    <p>Reclaim your everyday space by packing away your Christmas baubles, snow gear and all the other bits and bobs that you don't need all year round.</p>
	    </div>
	    <div class="third">
		    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icon-money.svg" alt="clear out your garage" />
		    <h3>Increase your home’s value</h3>
		    <p>Converting your unused attic space is an affordable renovation that not only results in desirable storage space but also adds value to your property.</p>
	    </div>
    </section>
    <div class="clear white">
    	  <a href="<?php echo home_url(); ?>/win-an-awesome-fishing-adventure/"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/comp-banner.jpg" alt="Win an Awesome Fishing Adventure with Attic Installations" /></a>

    </div>
    <div class="clear white">
	    <section class="cd-section lead">
	    	<h2>It couldn't be easier</h2>
	    	<p>With an emphasis on quality and value for money, Attic Installations can solve your home storage problems today. So why wait?</p>
	    	<a href="tel:+64 9 212 9970" class="button">Call us today</a>
	    </section>
    </div>
    
    <div class="gallery clear">
	    <?php $images = get_field('gallery');
			if( $images ): ?>
		   	<?php foreach( $images as $image ): ?>
		    	<img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
		    <?php endforeach; ?>
		<?php endif; ?>
		<div class="overlay">
		</div>
		<section class=" examples cd-section lead clear white">
	    	<h2>Examples of our work</h2>
	    	<p>Browse Attic Installations portfolio to take a look at how our discreet, professional installs are adding value to homes across Auckland.</p>
	    	<a href="<?php echo home_url(); ?>/our-work/" class="button">Our work</a>
	    </section>
	</div>
    

    
    <div class="clear">
	    <section class="cd-section lead">
	    	<h2>We're experienced</h2>
	    	<p>With over 12 years industry experience in home improvements, we pride ourselves on our customer service and space saving solutions. Find out more about Attic Installations below.</p>
	    	<a href="<?php echo home_url(); ?>/about-us/" class="button">About us</a>
	    </section>
    </div>
    
    
<?php get_footer(); ?>