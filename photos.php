<div id="photos" class="clear">
	<?php 
	$images = get_field('gallery');
	
	if( $images ): ?>

	        <?php foreach( $images as $image ): ?>
	 
	                <a href="<?php echo $image['url']; ?>" data-featherlight="image">
	                     <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
	                </a>
	                <p><?php echo $image['caption']; ?></p>
	     
	        <?php endforeach; ?>

	<?php endif; ?>
</div>
<div class="clear"></div>
<h1 class="title"><span>Instagram</span></h1>
<div id="instafeed"></div>
<div class="clear"></div>